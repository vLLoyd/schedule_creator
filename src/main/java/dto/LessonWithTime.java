package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class LessonWithTime {
    private String dayOfWeek;
    private WeekType weekType;
    private String[] weekNumber;
    private String startTime;
    private String endTime;
    private String lessonName;
    private String lessonType;
    private String professor;
    private String place;

    public enum WeekType {
        I, II
    }
}
