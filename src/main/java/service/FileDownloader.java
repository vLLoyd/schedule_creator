package service;

import java.io.File;

interface FileDownloader {
    String downloadFile(String url);
}
