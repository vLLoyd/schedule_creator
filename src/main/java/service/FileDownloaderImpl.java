package service;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;

public class FileDownloaderImpl implements FileDownloader {
    private static final int CONNECTION_TIMEOUT = 10_000;   // 10 sec timeout
    private static final int READ_TIMEOUT = 20_000;         // 20 sec timeout

    public String downloadFile(String url) {
        try {
            final File file = new File("src/main/resources/schedule.xlsx");
            FileUtils.copyURLToFile(new URL(url),
                    file,
                    CONNECTION_TIMEOUT,
                    READ_TIMEOUT
                    );
            System.out.println("File downloaded successfully");
            return file.getPath();
        } catch (Exception e) {
            System.out.println("Error downloading file");
            return null;
        }
    }
}

//https://www.mirea.ru/upload/medialibrary/cb4/IIT_1k_mag_18_19_vesna.xlsx
