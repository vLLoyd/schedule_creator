package service;

import dto.LessonWithTime;

import java.util.List;

public interface ParseScheduleService {
    List<LessonWithTime> parseScheduleFromXlsx(String groupName) throws Exception;
}
