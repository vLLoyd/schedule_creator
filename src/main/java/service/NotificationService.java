package service;

import dto.LessonWithTime;

import java.time.LocalDate;

public interface NotificationService {
    boolean setWeekNumbers(LocalDate beginningOfSemester);
    boolean createEventsForOneLesson(LessonWithTime lesson);
}
