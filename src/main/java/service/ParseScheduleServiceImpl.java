package service;

import dto.LessonWithTime;
import org.apache.poi.ss.usermodel.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ParseScheduleServiceImpl implements ParseScheduleService {
    private static final String EXCEL_FILE_NAME = "schedule.xlsx";
    private Integer groupColumnIndex;


    @Override
    public List<LessonWithTime> parseScheduleFromXlsx(String groupName) {
        final InputStream in = ParseScheduleServiceImpl.class.getResourceAsStream("/" + EXCEL_FILE_NAME);
        final Workbook wb;

        try {
            wb = WorkbookFactory.create(in);
        } catch (IOException e) {
            System.err.println(e.toString());
            return Collections.emptyList();
        }

        final Sheet firstSheet = wb.getSheetAt(0);
        final Row rowWithGroupName = firstSheet.getRow(1);
        final Iterator<Cell> cellIteratorForGroup = rowWithGroupName.cellIterator();

        while (cellIteratorForGroup.hasNext()) {
            Cell cell = cellIteratorForGroup.next();
            if (cell.getCellType() != CellType.STRING) continue;
            if (cell.getStringCellValue().equals(groupName)) {
                groupColumnIndex = cell.getColumnIndex();
            }
        }

        if (groupColumnIndex == null) {
            return Collections.emptyList();
        }
        List<LessonWithTime> scheduleList = new LinkedList<>();
        String currentDayOfWeek = "";
        String currentStartTime = "";
        String currentEndTime = "";
        Cell dayOfWeekCell;
        Cell startTimeCell;
        Cell endTimeCell;
        Iterator<Row> iterator = firstSheet.rowIterator();

        while(iterator.hasNext()) {

            Row currentRow = iterator.next();
            if (currentRow.getCell(groupColumnIndex + 3).getCellType() == CellType.NUMERIC) {
                continue;
            }
            if (currentRow.getCell(2).getStringCellValue().trim().equals("Начальник  УМУ")) {
                break;
            }

            dayOfWeekCell = currentRow.getCell(0);
            startTimeCell = currentRow.getCell(2);
            endTimeCell = currentRow.getCell(3);

            currentDayOfWeek = dayOfWeekCell.getCellType().equals(CellType.STRING) ? dayOfWeekCell.getStringCellValue() : currentDayOfWeek;
            currentStartTime = startTimeCell.getCellType().equals(CellType.STRING) ? startTimeCell.getStringCellValue() : currentStartTime;
            currentEndTime = endTimeCell.getCellType().equals(CellType.STRING) ? endTimeCell.getStringCellValue() : currentEndTime;

            LessonWithTime lesson = LessonWithTime.builder()
                    .dayOfWeek(currentDayOfWeek)
                    .startTime(currentStartTime)
                    .endTime(currentEndTime)
                    .weekType(currentRow.getCell(4).getStringCellValue().equals("I") //check for several rows
                            ? LessonWithTime.WeekType.I
                            : LessonWithTime.WeekType.II)
                    .lessonName(currentRow.getCell(groupColumnIndex).getStringCellValue())
                    .lessonType(currentRow.getCell(groupColumnIndex + 1).getStringCellValue())
                    .professor(currentRow.getCell(groupColumnIndex + 2).getStringCellValue())
                    .place(currentRow.getCell(groupColumnIndex + 3).getStringCellValue())
                    .build();

            scheduleList.add(lesson);
            System.out.println(lesson);
        }

        scheduleList = scheduleList.stream()
                .filter(it -> it.getLessonName() != null && !it.getLessonName().equals("") && !it.getDayOfWeek().equals(""))
                .peek(this::getLessonWeeksIfExist)
                .peek(this::trimName)
                .collect(Collectors.toList());
        return scheduleList;


    }

    private void getLessonWeeksIfExist(LessonWithTime lesson) {
        String lessonName = lesson.getLessonName().trim();
        lesson.setWeekNumber(lessonName.replaceAll("[^-?0-9]+", " ")
                .trim()
                .split(" "));
    }

    private void trimName(LessonWithTime lesson) {
        String lessonName =
                lesson.getLessonName()
                        .replaceAll("[0-9]+|н\\.| н |,", "")
                        .trim();
        lesson.setLessonName(lessonName);
    }
}
