package service;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import dto.LessonWithTime;
import utils.CreateCalendarApiService;

import java.time.LocalDate;
import java.time.Month;

public class NotificationServiceImpl implements NotificationService {

    CreateCalendarApiService apiService = new CreateCalendarApiService();
    Calendar calendar = apiService.createCalendarService();

    @Override
    public boolean setWeekNumbers(LocalDate beginningOfSemester){
        LocalDate endingOfSemester = getEndingOfSemester(beginningOfSemester);

        EventDateTime eventDateTime = new EventDateTime();
        DateTime dateTime = new DateTime(true, 0L, 3);
        eventDateTime.setDate(dateTime);
        Event event = new Event();
        event.setStart(eventDateTime);
        event.setEnd(eventDateTime);
        try {
            calendar.events().insert("primary", event);
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    @Override
    public boolean createEventsForOneLesson(LessonWithTime lesson) {
        return false;
    }

    private LocalDate getEndingOfSemester(LocalDate beginningOfSemester) {
        if (beginningOfSemester.getMonth() == Month.FEBRUARY) {
            return LocalDate.of(beginningOfSemester.getYear(), 5, 31);
        } else if (beginningOfSemester.getMonth() == Month.SEPTEMBER) {
            return LocalDate.of(beginningOfSemester.getYear(), 12, 31);
        }
        return null;
    }
}
