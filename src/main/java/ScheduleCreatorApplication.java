import service.FileDownloaderImpl;
import service.ParseScheduleServiceImpl;

public class ScheduleCreatorApplication {

    public static void main(String... args) {

        FileDownloaderImpl fileDownloader = new FileDownloaderImpl();
        fileDownloader.downloadFile("https://www.mirea.ru/upload/medialibrary/cb4/IIT_1k_mag_18_19_vesna.xlsx");
        ParseScheduleServiceImpl parseService = new ParseScheduleServiceImpl();
        try {
            parseService.parseScheduleFromXlsx("ИВМО-02-18");
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}